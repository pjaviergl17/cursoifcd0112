/* tipo javascript clásico, función con nombre llamada por un evento onclick html */
  const os = require ('os')
  function architec() {
      document.getElementById("archi").innerText =os.arch()    
  }

  /* mezcla: función con nombre llamada por un evento onclick html y uso de jquery para
  dar valor a los elementos */
  function sisop() {
  $('#sisop').text(os.type())
  $('#versisop').text(os.release())
}
 function memoria() {
    var txt = $('#memo').text() == "" ? (os.totalmem()/1024/1024/1024).toFixed(2) : ""
     $('#memo').text(txt)

    // $('#memo').text( (os.totalmem()/1024/1024/1024).toFixed(2)   )
    //$('#memlib').text( (os.freemem()/1024/1024/1024).toFixed(2)   )
    
    var jmem = $('#memlib').text()=="" ? (os.freemem()/1024/1024/1024).toFixed(2) : ""
    $('#memlib').text(jmem)
    
  }
  /* uso de jquery puro, y funciones arrow,  */ 
  
 $("#btn-cpu").click(()=> {
        var micro= $('#cpu').text()=="" ? (JSON.parse(JSON.stringify( os.cpus()[0].model) )) : ""
        $("#cpu").text(micro)

        var frecu= $('#frq').text()=="" ? (JSON.parse(JSON.stringify( os.cpus()[0].speed) )) : ""
        $("#frq").text(frecu)



        //$("#cpu").text(JSON.parse(JSON.stringify( os.cpus()[0].model) ));
        //$("#frq").text(JSON.parse(JSON.stringify( os.cpus()[0].speed)));
      })

 $('#btn-red').click(()=> {

        var red1 = $("#red").text()== "" ? (JSON.stringify(os.networkInterfaces().eth0[0].address) ) : ""
        $('#red').text(red1)
        var mascara1 = $('#mascara').text() =="" ? (JSON.stringify(os.networkInterfaces().eth0[0].netmask) ) : ""
        $('#mascara').text(mascara1)
        var mac1 = $('#mac').text() =="" ? (JSON.stringify(os.networkInterfaces().eth0[0].mac) ) : ""
        $('#mac').text(mac1)




 
    //$("#red").text(JSON.stringify(os.networkInterfaces().eth0[0].address) );
    //$("#mascara").text(JSON.stringify(os.networkInterfaces().eth0[0].netmask) );
    //$("#mac").text(JSON.stringify(os.networkInterfaces().eth0[0].mac) );
  }) 


 $("#btn-discos").click(()=>{
  const exec = require('child_process').exec;
  var cmd = 'df -h | grep /dev/sd > hd.txt' 
  exec(cmd, (error, stdout, stderr) => {
    
  $("#discos").load("hd.txt")
  //var discos1 = $("#discos").load() =="" ? load("hd.txt") : ""

});

 });



 

