#!/bin/bash
informe=informe.txt
 echo "" > $informe

 echo "                         INFORME DEL SISTEMA" >> $informe
 echo "                         =================== " >>$informe 
 function cpu {
 	echo "" >> $informe
 	echo "" >> $informe
 	echo " PROCESADOR/ES" >> $informe
 	echo "" >> $informe
 	echo "" >> $informe
 	lscpu >> $informe
 }
 function memoria {
 	echo "" >> $informe
 	echo "" >> $informe
 	echo " MEMORIA" >> $informe
 	echo "" >> $informe
 	echo "" >> $informe
 	free -h >> $informe


 }
 function bus {
 	echo "" >> $informe
 	echo "" >> $informe
 	echo " BUS PCI" >> $informe
 	echo "" >> $informe
 	echo "" >> $informe
 	lspci >> $informe

 }
 function vga {
 	echo "" >> $informe
 	echo "" >> $informe
 	echo " VGA INSTALADA" >> $informe
 	echo "" >> $informe
 	echo "" >> $informe
 	lspci | grep VGA >> $informe

 }
 # para la función discos se puede instalar el comando lsscsi 
 # y usar otro también que es lsblk -fm
function discos {
	echo "" >> $informe
 	echo "" >> $informe
 	echo " ALMACENAMIENTO DEL SISTEMA" >> $informe
 	echo "" >> $informe
 	echo "" >> $informe
	df -h >> $informe
}

function email {
	control=5                                     #  defino tan baja la ocupación para verificar que realize el proceso
	unidad=`df -h | grep /dev/sdb2 | cut -c 34-36` # en el live, esta es la unidad que quiero controlar 
                                                   # y las posiciones que devuelve el comando                                                 
	if [ $unidad -ge $control ]
		then
			echo "mensaje de alerta enviado"       # no es necesario,se supone que la ejecución quedará programada, es solo para depuración
			echo " La unidad está al '$unidad%' de ocupación " | mail -s "Alerta ocupación disco" javiergl2016@gmail.com
				
		else
			echo "Unidad dentro de parámetros correctos"
	fi
}


cpu
memoria
bus
vga
discos
email



