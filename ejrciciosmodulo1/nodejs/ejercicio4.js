/* instalar el rsyncwrapper con mpm en la carpeta donde haga el script, (probé a hacer la instalación global y no me funcionaba) 
ahí me creará la instalación una carpeta que contiene node_ modules 
importantes. Tengo 2 opciones , o ejecutar el script en la misma carpeta donde estan los nuevos node_modules o 
copiar estos a la ruta incluida en el PATH, también claro,
el node ,npm  y node_modules a sus respectivas carpetas 
Para que la conexión ssh funcione sin contraseña debemos anteriormente 
haber copiado las claves al servidor. 

crontab -e :
00 19 * * * /usr/local/bin/node /media/user/DATOS/pruebas/pruebasnodejs/ejercicio4.js 
*/


var rsync= require('rsyncwrapper');
rsync({
    src: "javier@192.168.100.76:/home/javier",
    
    /* scr: ["javier@192.168.100.76:/home/javier", "javier@192.168.100.76:/var/www/html/public-javier]
    formato para establecer varias rutas de origen */

    dest: "/media/user/DATOS/backup-server/",
    ssh: true,
    recursive: true,
    sshCmdArgs:["-o StrictHostKeyChecking=no"],

     
},function (error,stdout,stderr) {
    console.log(error,stdout,stderr)
    if ( error ) {
        
        console.log("error de rsync");
    } else {
        console.log("todo correcto")
         
    }
});