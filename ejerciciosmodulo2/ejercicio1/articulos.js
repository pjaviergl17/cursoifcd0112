//const database = 'http://192.168.100.76:5984/jj/'
const database = 'http://192.168.100.71:5984/javiercdb/'

var defaultAjax={
	type: 'get',
	url: database + '_all_docs?include_docs=true',
	dataType: 'json',
	contentType: 'application/json; charset=utf-8'
	}

function getArticulos(){
	$('#selector').html('')
	defaultAjax.type = 'get'
	defaultAjax.url = database + '_all_docs?include_docs=true',
	$.ajax(defaultAjax).done(function(data){
		$.each(data.rows,function(idx,row){
			console.log(row.doc._id, row.doc.descripcion, row.doc.seccion, row.doc.familia, row.doc.marca, row.doc.subfamilia,row.doc.pvp );
			let html =`
			<option value="${row.id}">
			${row.doc.seccion}, ${row.doc.familia}, ${row.doc.subfamilia}, ${row.doc.marca}
			, ${row.doc.descripcion}, ${row.doc.pvp}
			</option>
			`
			$('#selector').append(html)
		});

	});
}

function crear(){
	var obj = { 
		seccion: $('#seccion').val(),
		familia: $('#familia').val(),
		subfamilia: $('#subfamilia').val(),
		descripcion: $('#descripcion').val(),
		marca: $('#marca').val(),
		pvp: $('#pvp').val(),

		
	}
	$.ajax({type: 'post', 
		url: database , 
		data: JSON.stringify(obj) ,
		dataType: 'json',
		contentType: 'application/json; charset=utf-8'})
		.done( function(data){ getArticulos()	})
		.fail( function(data){ alert( JSON.parse(data.responseText).reason    ) });	

}



function cambiaCombo(){

	defaultAjax.url = database + $('#selector option:selected').val()
	defaultAjax.type = 'get'
	$.ajax(defaultAjax).done(function(data){
		console.log(data)
		$('#seccion').val(data.seccion)
		$('#familia').val(data.familia)
		$('#subfamilia').val(data.subfamilia)
		$('#descripcion').val(data.descripcion)
		$('#marca').val(data.marca)
		$('#pvp').val(data.pvp)
		$('#id').val(data._id)
		$('#rev').val(data._rev)

	})
}

function modificar(){

	$.ajax({
		type: 'post',
		url: database,
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify({
			_id: $('#id').val(), 
			_rev: $('#rev').val(),
			seccion:$('#seccion').val(),
			familia:$('#familia').val(),
			subfamilia:$('#subfamilia').val(),
			descripcion:$('#descripcion').val(),
			marca:$('#marca').val(),
			pvp:$('#pvp').val()

			
			}),
			success: function(){ getArticulos() }
		
	})
	//getArticulos()
}

function borrar(){

	$.ajax({
		type: 'DELETE',
		url: database + $('#id').val() + "?rev=" + $('#rev').val(),
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify({

			_id: $('#id').val(), 
			_rev: $('#rev').val(),
			seccion:$('#seccion').val(),
			familia:$('#familia').val(),
			subfamilia:$('#subfamilia').val(),
			descripcion:$('#descripcion').val(),
			marca:$('#marca').val(),
			pvp:$('#pvp').val()
			}),
			success: function(){ getArticulos() }

	})	
	
}

function limpiar(){
	
		$('#seccion').val('')
		$('#familia').val('')
		$('#subfamilia').val('')
		$('#descripcion').val('')
		$('#marca').val('')
		$('#pvp').val('')
		$('#id').val('')
		$('#rev').val('')


}	

$(document).ready( getArticulos)
