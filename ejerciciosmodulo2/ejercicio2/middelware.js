var express = require('express');
const favicon = require('express-favicon');
var mysql = require('mysql');
var app = express();
var request = require('request');
var bodyParser = require('body-parser');
// ***CONEXIÓN CLASE **

var connection = mysql.createConnection({
  host     : '192.168.100.71',
  user     : 'root',
  password : 'a',
  database: 'appjgl'
});


// *** CONEXIÓN CASA ***

// var connection = mysql.createConnection({
//   host     : 'localhost',
//   user     : 'root',
//   password : 'my',
//   database: 'appjgl'
// });
//app.use(favicon(__dirname + '/public/favicon.ico'));
//app.use(favicon('./public/favicon.ico'));
app.use(require('body-parser').json()); 

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  next();
});

connection.connect();

app.use( express.static('public') );
app.use( bodyParser.json() );  
app.use( bodyParser.urlencoded({ extended: true }) ); 



// prueba de listas
app.get('/listas', function (req, res) {   
  let sql =`  select id_lista, nombre, date_format(fecha, '%Y-%m-%d') as fecha from listas;`  
  connection.query(sql, function (error, result) {           
    res.json(result);
   // res.write(json.stringify(result));

  });  
});

// lista por identificador
app.get('/listas/:id', function (req, res) { 
  //console.log(req.params.id)
  let sql =`  select id_lista, nombre, date_format(fecha, '%Y-%m-%d') as fecha from listas where id_lista = ${req.params.id} `    
  //connection.query('select * from listas where id_lista = ${req.params} ?;', function (error, result) {           
    connection.query(sql, function(error,result,fields) {
    res.json(result[0])
    //console.log(result[0])

   // res.write(json.stringify(result));

  });  
});

app.get('/articulos/:id', function (req, res) { 
  console.log('hola' + req.params.id)
  let sql =`  select * from articulos where mid(id_arti,1,6)=${req.params.id} `    
  //connection.query('select * from listas where id_lista = ${req.params} ?;', function (error, result) {           
    connection.query(sql, function(error,result,fields) {
      res.json(result)
   
   // res.json(result[0])
    //console.log(result[0])

   // res.write(json.stringify(result));

  });  
});

app.get('/articulos/completarlista/:id', function (req, res) { 
  console.log('hola' + req.params.id)
  let sql =`  select * from articulos where id_arti = ${req.params.id} `    
  //connection.query('select * from listas where id_lista = ${req.params} ?;', function (error, result) {           
    connection.query(sql, function(error,result,fields) {
      res.json(result[0])
      console.log(result)
   
   
  });  
});

app.get('/pedidos/:id', function (req, res) { 
  console.log(req.params.id)      
 //var id = req.params.id
 let sql =`select lp.id_lista, lp.id_arti, ar.descri_arti, lp.cantidad,  lp.precio, lp.total from lineaspedido lp inner join articulos  ar on (lp.id_lista= ${req.params.id} and lp.id_arti=ar.id_arti); `
  connection.query(sql, function (error, result) { 
   console.log(result)          
    res.json(result);
   // res.write(json.stringify(result));

  });  
});

app.get('/campos', function (req, res) {       
  connection.query('describe articulos;', function (error, result) {           
    res.json(result);
   // res.write(json.stringify(result));

  });  
});
app.get('/articulos', function (req, res) {      	
  connection.query('SELECT * FROM articulos;', function (error, result) {          	
    res.json(result);
   // res.write(json.stringify(result));

  });  
});

app.get('/tablas' , function (req, res){
  connection.query('SELECT table_name FROM information_schema.TABLES WHERE TABLE_SCHEMA  = "appjgl" ORDER BY TABLE_NAME;' , 
      function (error, result) {
        res.json(result);
      });
});

app.post('/grabarnewlista' , function (req, res){
  connection.query('Insert into listas (nombre, fecha) values (?, ?)', [req.body.nombre, req.body.fecha] ,
      function (error, result) {
        res.json(result);
      });
});

app.delete('/borrarLista/:id', function (req, res){
  let sql=` DELETE FROM appjgl.listas WHERE id_lista = ${req.params.id}; `
  console.log(sql)
  connection.query(sql, function(error, result) {
    res.json(result);
  });

});

app.post('/grabarlineapedido', function(req, res){
  console.log('este es elrequest' + req)
  connection.query('insert into lineaspedido (id_lineas,id_lista,id_arti,cantidad,precio,total) values (?,?,?,?,?)', [req.body.id_lineas, req.body.id_lista, req.body.id_arti, req.body.precio, req.body.cantidad, req.body.total], function(error, result){
    res.json(result);
  })
})



app.listen(3000, function () { 
	console.log('!!! Servidor express preparado para atender peticiones en puerto 3000 !!!');
});
